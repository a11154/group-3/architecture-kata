workspace "Getting Started" "This is a model of my software system." {

    model {

        pacientUser = person "Paciente" "Paciente con alguna sintomatología" "Person"
        medicUser = person "Personal médico" "Personal que se encargara de conversar con el usuario" "Person"
        googleCloudSystem = softwareSystem "Google Cloud Platform" "Servicios de Google para correo y geolocalización" "External"
        
        
        
        softwareSystem = softwareSystem "Sistema de ayuda médica" "Permite a los clientes preguntar a personal médico acerca de posibles problemas de salud"{
           webPage = container "Página Web" "Muestra todo el contenido estático del centro médico" "React" "Web Browser"
           spaUsers = container "Single Page Application de Usuario" "Provee toda la funcionalidad para que el cliente se pueda comunicar con el personal médico, además de mostrar las ubicaciones" "React" "Web Browser"
           spaMedic = container "Single Page Application de personal médico" "Provee toda la funcionalidad para que el personal médico conteste las preguntas de los usuarios" "React" "Web Browser"
           database = container "Base de Datos" "Almacena la historia clínica de los pacientes, historiales de conversaciones, usuarios" "PostgreSQL" "Database"
           apiAplication = container "Aplicacion API" "Expone un REST API para realizar todas las operaciones" "ExpressJS con NodeJS" {
               userCreation = component "Controlador creación de usuarios" "Controla las peticiones para la creacion de usuarios" "Middleware Express"
               userAutentication = component "Controlador Autenticación de usuarios" "Controla las peticiones de Log In" "Middleware Express"
               chatBot = component "Controlador Chatbot" "Controla el sistema de chatbot" "Middleware Express"
               chatRooms = component "Controlador salas de chat" "Controla la conexión/creación de las salas" "Middleware Express"
               medicTriage = component "Controlador triage médico" "Controla las peticiones para el registro de sintomatologia" "Middleware Express"
               geoInformation = component "Controlador informacion geografica" "Controla las peticiones de las ubicaciones" "Middleware Express"
               
               userCreationService = component "Servicio de creación de usuarios" "Se encarga de conectarse a DB y crear los dataframes" "NodeJS Module"
               autenticationService = component "Servicio de autenticación" "Se encarga de validar la validez de los JWT y de obtener información del usuarion en DB" "NodeJS Module"
               chatBotService = component "Servicio de ChatBot" "Se encarga de conectarse a DB para consuiltar datos de paciente y tiene el arbol de decisiones" "NodeJS Module"
               websocketsService = component "Servicio de Websockets" "Se encarga de manejar las salas y las conexiones para las sesiones de chat" "NodeJS Module"
               mailService = component "Servicio de Correo" "Se encarga de conectarse a Google Cloud Platform para solicitar el envio de correo" "NodeJS Module"
               geolocationService = component "Servicio de Geolocalización" "Se encarga de conectarse a la API de Google, renderizar los mapas y de obtener las ubicaciones" "NodeJS Module"
               
           }
        }
        /*Context level connections*/
        pacientUser -> softwareSystem "Genera preguntas hacia el personal médico usando"
        medicUser -> softwareSystem "Conversa con los usuarios utilizando"
        softwareSystem -> googleCloudSystem "Obtiene información geografica y envia correos usando"
        googleCloudSystem -> pacientUser "Envia correos a"  
        
        /*Container level connections*/
        apiAplication -> database "Lectura y escritura de datos hacia" "TCP"
        webPage ->  spaUsers "Entrega el contenido al navegador del usuario"
        webPage ->  spaMedic "Entrega el contenido al navegador del usuario"
        spaUsers -> googleCloudSystem "Renderiza mapas usando"
        spaUsers -> apiAplication "Hace llamadas a la API" "HTTP / Websockets"
        spaMedic -> apiAplication "Hace llamadas a la API" "HTTP / Websockets"
        medicUser -> webPage "Visita la página web"
        pacientUser -> webPage "Visita la página web"
        apiAplication -> googleCloudSystem "Envia correos usando"
        
        /*Component level connections */
        userCreation -> userCreationService "Utiliza"
        userAutentication -> autenticationService "Utiliza"
        chatBot -> chatBotService "Utiliza"
        chatRooms -> websocketsService "Utiliza"
        medicTriage -> mailService "Utiliza"
        geoInformation -> geolocationService "Utiliza"
               
        userCreationService -> database "Se conecta a"
        autenticationService -> database "Se conecta a"
        chatBotService -> database "Se conecta a"
               
        mailService -> googleCloudSystem "Envia correos utilizando"
        geolocationService -> googleCloudSystem "Renderiza mapas utilizando"
            
        spaUsers -> userCreation "Llamada a API" "HTTP"
        spaUsers -> userAutentication "Llamada a API" "HTTP"
        spaUsers -> chatBot "Llamada a API" "HTTP"
        spaUsers -> chatRooms "Llamada a API" "HTTP"
        spaUsers -> medicTriage "Llamada a API" "HTTP"
        spaUsers -> geoInformation "Llamada a API" "HTTP"
            
        spaMedic -> chatRooms "Llamada a API" "HTTP"
        spaMedic -> medicTriage "Llamada a API" "HTTP"
    }

    views {
        systemContext softwareSystem "SystemContext" "An example of a System Context diagram." {
            include *
            autoLayout lr
        }
        
        container softwareSystem "ContainerLevel" "Nivel de contendor de Sistema de ayuda médica"{
            include *
            autolayout lr
        }
        
        component apiAplication "APIAplication" "Aplicación API"{
            include *
            autoLayout
        }
        
        styles {
            element "Person" {
            background #08427b
                color #ffffff
                fontSize 22
                shape Person
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Database" {
                shape Cylinder
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "External"{
                color #000000
                background #eeeeee
            }
        }
    }
    
}