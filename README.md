# Kata arquitectura Am.I.Sck
Grupo 3

Integrantes

* Carlos Parraga
* Javier Loachamín
* Bryan Nicolas Cisneros
* Giovanny Criollo

## Acerca de

Este repositorio contiene todas las decisiones de arquitecturas, diagramas, requeriminetos, ADR, etc.

## Estructura

architecture-kata\adrs

- [ADR](/adrs/)  Contiene todas las decisiones de arquitectura

- [Diagramas C4](/Diagrama%20C4/)  Contiene el codigo del diagrama C4 escrito en structurizr Dsl

- [Diagrama de contexto ](/Diagrama%20C4/Context.png)  Contiene el diagrama de contexto

- [Diagrama de contenedor ](/Diagrama%20C4/Container.png)  Contiene el diagrama de contenedor

- [Diagrama de Contexto ](/Diagrama%20C4/Context.png)  Contiene el diagrama de contexto