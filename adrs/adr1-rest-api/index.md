# [REST API]

* Status: [accepted] 
* Deciders: [
    Carlos Parraga
    Nicolas Cisneros
    Giovanny Criollo
    Javier Loachamin
] <!-- optional -->
* Date: [2022-05-20] 

## Context and Problem Statement

[Se utilizo porque es una tecnología ya probada, suficiente para cumplir con lo requerido y fácil de implementar .]

## Decision Drivers

* [http]

## Considered Options

* [Tecnologia probada]
* [Facil de implementar]

## Decision Outcome

Chosen option: "[option 1]", because [justification. e.g., only option, which meets k.o. criterion decision driver | which resolves force force | … | comes out best (see below)].

### Positive Consequences <!-- optional -->

* [es una tecnología ya probada, suficiente para cumplir con lo requerido y fácil de implementar .]

### Negative Consequences <!-- optional -->

* [pese a que no se solicita la compartición de archivo va a requerir mas enfuerzo en caso de necesitar hacerlo.]

## Pros and Cons of the Options <!-- optional -->

### [option 1]

[example | description | pointer to more information | …] <!-- optional -->

* Buena, porque yaes probada
* Buena, porque es facil de implementar
