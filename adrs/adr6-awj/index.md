# autentificación JWT

* Status: [accepted]

## Context and Problem Statement

Se utiliza porque es una autentificación ya probada y que cumple con lo requerido para el sistema. 

## Decision Drivers 

* Autenticación JWT

## Considered Options

Metodo seguro de autenticación.

## Decision Outcome

Metodo de autentifación que otorgue seguridad al sistema.

### Positive Consequences <!-- optional -->

* Seguridad al momento de compartir información.
* Facil de implementar, un metodo ya probado.

### Negative Consequences <!-- optional -->

* Tamaños de los JWT
* Para cada llamado debe incluir el JWT


