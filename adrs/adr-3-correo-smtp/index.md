# Sistema de notificación por correo 

* Status: accepted
* Deciders:
    1. Carlos Parraga
    2. Giovanny Criollo
    3. Javier Loachamin
    4. Nicolas Cisneros
* Date: 2022-05-21

Technical Story: description

## Context and Problem Statement

El requerimiento del cliente es el de un boletín donde conste todas las locaciones cercanas a el

## Decision Drivers

* Buena reputación para que no sea marcado como SPAM

## Considered Options

* Gmail for bussiness
* Sendgrid
* Amazon SES

## Decision Outcome

Opción elegida: "Gmail for bussiness", debido a que se utilizara el SDK de Google para geolocalización, por consiguiente el proceso de integración sera mas sencillo.

### Negative Consequences <!-- optional -->

* Cerrado al SDK de Google

## Pros and Cons of the Options <!-- optional -->

### Gmail

* Bueno, gratis para esta aplicación en especifico
* Bueno, se integra con nuestros servicios actuales

### Sendgrid

* Buena, tiene una capa gratuita
* Buena, tiene una API intuitiva

### Amazon SES

* Buena, económica para envio masivo
* Mala, no se adapta a nuestros requerimientos
