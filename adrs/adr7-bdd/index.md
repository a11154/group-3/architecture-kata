# autentificación JWT

* Status: [accepted]

## Context and Problem Statement

 BDD relacional debido que se va a ingresar historial del paciente, enfermeros, doctores y se va a requerir reportes de los mismos, Además de ser un servicio gratuito y cumple lo requerido para la solución.

## Decision Drivers 

* BDD Relacional Postgres

## Considered Options

* sql: Costo y limitante de tamaño en version FREE
* MySql: Tiene menos funciones que postgres

## Decision Outcome

Se requiere una base relacional que cumpla con lo solicitado.

### Positive Consequences <!-- optional -->

* Bajos costos.
* Buena capacidad de almacenamiento.
* Escalable y confiable

### Negative Consequences <!-- optional -->

* No existe un soporte oficial
* Es mas lento en inserciones y actualizaciones para bases pequeñas en comparacion con otros.


