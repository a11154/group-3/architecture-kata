# Geolocalización usando Google Maps API

* Status: accepted 
* Deciders: 
    1. Carlos Parraga
    2. Giovanny Criollo
    3. Javier Loachamin
    4. Nicolas Cisneros
* Date: 2022-05-21 

Technical Story: description

## Context and Problem Statement

El requerimiento del cliente es el de un sistema que ayude a potenciales clientes a encontrar los diferentes puntos de atención médica

## Decision Drivers <!-- optional -->

* Debe tener sistema de navegación
* Alta disponibilidad
* Facilidad de uso

## Considered Options

* Google Maps API
* Here WeGo Maps
* Apple Maps

## Decision Outcome

Opcion elegida: "Google Maps API", debido a que está practicamente en todos los dispositivos moviles, tiene informacion geográfica de puntos de interés y es el estándar de facto para navegación y geolocalización

### Positive Consequences 

* Familiaridad con la plataforma

### Negative Consequences 

* Cerrado al SDK de Google

## Pros and Cons of the Options

### Google Maps API

* Buena, porque permite personalizacion de mapas
* Buena, porque está en todos los dispositivos
* Buena, porque tiene un motor de búsqueda excelente
* Mala, porque tiene una capa de pago y una gratuita
* Mala, porque la localizacion puede no estar disponible en todas las regiones

### Here WeGo Maps

* Buena, porque tiene un costo menor
* Buena, porque cuenta con modo offline
* Mala, porque la localizacion GPS es inexacta

### Apple Maps

* Buena, porque tiene varias caracteristicas interactivas
* Mala, porque solo esta disponible en dispositivos Apple
