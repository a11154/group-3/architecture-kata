# Single-page application(spa) para el personal medico
 * Status:  accepted
 * Deciders:
     1. Carlos Parraga
     2. Giovanny Criollo
     3. Javier Loachamin
     4. Nicolas Cisneros
 * Date: 21/05/2022 

## Context and Problem Statement

Se usara SPA debido a su versatilidad y modularidad, ademas que da una experiencia mas fluida a los usuarios

## Decision Drivers 

NextJS

### Positive Consequences <!-- optional -->

Alta disponibilidad en la CDN de Verce
Agnositica de la plataforma
Se puede crear clientes moviles y de escritorio de acuerdo 
a la necesidad del negocio

### Negative Consequences <!-- optional -->


Al momento no presenta ninguna consecuencia negativa

