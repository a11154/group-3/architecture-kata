# [real time chat]

* Status: [accepted] 
* Deciders: [
    Carlos Parraga
    Nicolas Cisneros
    Giovanny Criollo
    Javier Loachamin
] 
* Date: [2022-05-20] 

Technical Story: [description | ticket/issue URL] 

## Context and Problem Statement

[Se necesita un sistema de comunicacion en tiempo real, entre el personal y el paciente]

## Decision Drivers 

* [Socket.io]

## Considered Options

* [Chat en tiempo Real]
* [option 2]
* [option 3]
* … <!-- numbers of options can vary -->

### Positive Consequences <!-- optional -->

* [Facil implementacion, Funciona con Java, C++, Swift, Dart, Python, .NET, Open Source, Baja latencia ]


## Pros and Cons of the Options <!-- optional -->

* Buenas, porque facil implementar
* Buena, Open Source
* Bad, because [argument c]

